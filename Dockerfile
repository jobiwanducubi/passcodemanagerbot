FROM python:3.7.4-alpine3.10 as builder

WORKDIR /

RUN apk --update add --no-cache --virtual .build-deps gcc libc-dev libffi-dev openssl-dev

COPY app/requirements.txt /requirements.txt

RUN set -ex; \
        pip install -r requirements.txt
RUN apk del .build-deps

FROM builder

COPY app /app
WORKDIR /app

ENTRYPOINT [ "python3", "bot.py" ]
