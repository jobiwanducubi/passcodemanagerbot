#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import os

from resources import Database, PasscodeManager, NoPasscodeAvailableException

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler, PicklePersistence, CallbackQueryHandler

# Enable logging
logging.basicConfig(
  format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
  level=logging.INFO
)

logger = logging.getLogger(__name__)
database = None

CHOOSING, CHECKING_NICKNAME, CONFIRMING_NICKNAME, TYPING_NEW_PASSCODE = range(4)
PASSCODE_VALID, PASSCODE_ALREADY_USED, PASSCODE_INVALID, PASSCODE_FULLY_REDEEMED = range(4)
internal_passcode_states = [
  'ok', 'ar', 'nv', 'fr'
]
passcode_states = [
  'valide', 'déjà utilisé', 'invalide', 'fully redeemed'
]


choosing_keyboard = [
  ['Passcode aléatoire', 'Nouveau passcode'],
  ['Situation avec le bot', 'Définir mon pseudo']
]
choosing_markup = ReplyKeyboardMarkup(choosing_keyboard, one_time_keyboard=True)

yesno_keyboard = [['Oui', 'Non']]
yesno_markup = ReplyKeyboardMarkup(yesno_keyboard, one_time_keyboard=True)

passcode_keyboard = [
  [
    InlineKeyboardButton(
      text="Valide",
      callback_data=internal_passcode_states[PASSCODE_VALID]
    ),
    InlineKeyboardButton(
      text="Invalide",
      callback_data=internal_passcode_states[PASSCODE_INVALID]
    ),
    InlineKeyboardButton(
      text="Déjà utilisé",
      callback_data=internal_passcode_states[PASSCODE_ALREADY_USED]
    ),
    InlineKeyboardButton(
      text="Fully redeemed",
      callback_data=internal_passcode_states[PASSCODE_FULLY_REDEEMED]
    )
  ]
]
passcode_markup = InlineKeyboardMarkup(passcode_keyboard)


def passcode_buttons(update, context):
  if not 'nickname' in context.user_data:
    return

  passcode_manager = PasscodeManager(database)

  nickname = context.user_data['nickname']
  
  query = update.callback_query
  passcode = query.message.text
  data = query.data
    
  if data in internal_passcode_states:
    internal_state = data

    passcode_manager.set_passcode_state(
      nickname,
      passcode,
      internal_state
    )

    state = passcode_states[
      internal_passcode_states.index(data)
    ]

    query.edit_message_text(
      "{} [{}]".format(passcode, state)
    )


def start(update, context):
  user_data = context.user_data

  if 'nickname' in user_data:
    # Already known
    update.message.reply_text(
      "Que souhaites-tu faire ?", 
      reply_markup=choosing_markup
    )
  else:
    update.message.reply_text(
      "Nouvel utilisateur détecté.\n"
      "\n"
      "Attention, le bot est maintenant connecté au fichier principal !\n"
      "Demande à LeNeptunien pour plus d'informations.\n"
      "\n"
      "Tu devrais définir ton pseudo pour commencer ;-)\n"
      "\n"
      "La persistence est maintenant activée, je ne t'oublierai plus !",
      reply_markup=choosing_markup
    )

  return CHOOSING


def type_new_nickname(update, context):
  update.message.reply_text(
    'Quel pseudo souhaites-tu utiliser ?',
    reply_markup=ReplyKeyboardRemove()
  )

  return CHECKING_NICKNAME


def check_nickname(update, context):
  passcode_manager = PasscodeManager(database)
  nickname = update.message.text

  context.user_data['nickname'] = nickname

  text = 'Le pseudo `{}` '.format(nickname)

  if nickname in passcode_manager.users:
    text += 'est déjà connu.'
  else:
    text += 'n\'existe pas.'
        
  update.message.reply_text(
    text + ' Souhaites-tu l\'utiliser ?', 
    parse_mode='Markdown', 
    reply_markup=yesno_markup
  )

  return CONFIRMING_NICKNAME


def confirm_nickname(update, context):
  text = update.message.text

  if 'nickname' in context.user_data:
    if text == 'Non':
      del context.user_data['nickname']
    
      update.message.reply_text(
        'Aucun pseudo défini.'
      )

    else:
      passcode_manager = PasscodeManager(database)
      nickname = context.user_data['nickname']

      if not nickname in passcode_manager.users:
        passcode_manager.add_user(nickname)
      
      update.message.reply_text(
        'Le pseudo est maintenant défini par {}.'.format(nickname),
        parse_mode='Markdown'
      )

  return start(update, context)


def type_new_passcode(update, context):
  if not 'nickname' in context.user_data:
    return define_nickname(update, context)
  
  update.message.reply_text(
    'Quel est le passcode ?',
    reply_markup=ReplyKeyboardRemove()
  )

  return TYPING_NEW_PASSCODE


def new_passcode(update, context):
  if not 'nickname' in context.user_data:
    return define_nickname(update, context)
  
  passcode_manager = PasscodeManager(database)
  passcode = update.message.text.lower()

  if passcode in passcode_manager.passcodes:
    update.message.reply_text(
      'Ce passcode existe déjà !'
    )
  else:
    passcode_manager.add_passcode(passcode)

    update.message.reply_text(
      'Le nouveau passcode a été enregistré !'
    )
    
    update.message.reply_text(
      '`{}`'.format(passcode), 
      parse_mode='Markdown',
      reply_markup=passcode_markup
    )
  
  return start(update, context)


def random_passcode(update, context):
  if not 'nickname' in context.user_data:
    return define_nickname(update, context)

  passcode_manager = PasscodeManager(database)
  nickname = context.user_data['nickname']
  
  try:
    passcode = passcode_manager.get_random_passcode(nickname)

    update.message.reply_text(
      '`{}`'.format(passcode), 
      parse_mode='Markdown', 
      reply_markup=passcode_markup
    )
  except NoPasscodeAvailableException as e:
    update.message.reply_text(
      'Aucun passcode disponible'
    )
  
  return start(update, context)


def display_infos(update, context):
  nickname = '???'
  available_passcodes_count = 0
  
  if 'nickname' in context.user_data:
    nickname = context.user_data['nickname']

    passcode_manager = PasscodeManager(database)
    available_passcodes_count = len(passcode_manager.users[nickname])            
  
  update.message.reply_text(
    "Pseudo défini : {}\n"
    "Nombre de passcodes à évaluer : {}"
    "".format(nickname, available_passcodes_count),
    parse_mode='Markdown',
    reply_markup=choosing_markup
  )
  
  return CHOOSING


def define_nickname(update, context):
  update.message.reply_text(
    'Définis d\'abord le pseudo utilisé dans la feuille de calcul !',
    parse_mode='Markdown', 
    reply_markup=choosing_markup
  )

  return CHOOSING

def done(update, context):
  user_data = context.user_data
    
  update.message.reply_text(
    "Désolé, je n'ai pas compris. /start si tu souhaites discuter à nouveau !",
  )

  return ConversationHandler.END


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
  conv_handler = ConversationHandler(
    entry_points=[
      CommandHandler(
        'start',
        start
      )
    ],
    states={
      CHOOSING: [
        CommandHandler(
          'start',
          start
        ),
        MessageHandler(
          Filters.regex('^Passcode aléatoire$'),
          random_passcode
        ),
        MessageHandler(
          Filters.regex('^Nouveau passcode$'),
          type_new_passcode
        ),
        MessageHandler(
          Filters.regex('^Situation avec le bot$'),
          display_infos
        ),
        MessageHandler(
          Filters.regex('^Définir mon pseudo$'),
          type_new_nickname
        )
      ],
      CHECKING_NICKNAME: [
        MessageHandler(
          Filters.text,
          check_nickname
        )
      ],
      CONFIRMING_NICKNAME: [
        MessageHandler(
          Filters.text,
          confirm_nickname
        )
      ],
      TYPING_NEW_PASSCODE: [
        MessageHandler(
          Filters.text,
          new_passcode
        )
      ]
    },
    fallbacks=[
      MessageHandler(
        Filters.text,
        done
      )
    ],
    name="conversation",
    persistent=True
  )

  pp = PicklePersistence(filename='persistence/passcodemanagerbot')
  telegram_bot_token = os.getenv("TELEGRAM_BOT_TOKEN")
  updater = Updater(token=telegram_bot_token, persistence=pp, use_context=True)

  button_handler = CallbackQueryHandler(
    passcode_buttons
  )

  dp = updater.dispatcher
  dp.add_handler(conv_handler)
  dp.add_handler(button_handler)
  dp.add_error_handler(error)

  # Start the Bot
  updater.start_polling()

  updater.idle()

if __name__ == '__main__':
  #with open('credentials.json', 'r') as f:
  #  json_keyfile_dict = json.load(f)
  
  google_credentials = os.getenv("GOOGLE_CREDENTIALS")
  spreadsheet = os.getenv("SPREADSHEET")

  json_keyfile_dict = json.loads(google_credentials)
  database = Database(json_keyfile_dict, spreadsheet)
  
  main()

