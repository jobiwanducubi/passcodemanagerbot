import gspread

from datetime import datetime, timedelta
from oauth2client.service_account import ServiceAccountCredentials


class Database(object):
  def __init__(self, json_keyfile_dict, sheetname):
    scope = [
      'https://spreadsheets.google.com/feeds',
      'https://www.googleapis.com/auth/spreadsheets',
      'https://www.googleapis.com/auth/drive.file',
      'https://www.googleapis.com/auth/drive'
    ]
    
    self.last_authorization_time = datetime.now()
    self.credentials = ServiceAccountCredentials.from_json_keyfile_dict(json_keyfile_dict, scope)
    self.client = None
    self.spreadsheet = None
    self.sheetname = sheetname

  def authorize(self):
    self.client = gspread.authorize(self.credentials)
    self.spreadsheet = self.client.open(self.sheetname).sheet1

  def get_sheet(self):
    expired = self.last_authorization_time + timedelta(minutes=10) < datetime.now()

    # Do we need an authorization?
    if expired or self.spreadsheet is None:
      self.authorize()
      self.last_authorization_time = datetime.now()

    return self.spreadsheet  
