import gspread
import random

class PasscodeManager(object):
  def __init__(self, database):
    self.sheet = database.get_sheet()

    # All passcodes
    self.passcodes = []
    # Only valid passcodes for each users
    self.users = {}
    self.heads = []

    self.read_all()
  
  def read_all(self):   
    self.heads = self.sheet.row_values(1)
    
    """
    > 0: passcode (text)
    > 1: - (ignored)
    > 2: global state
    > 3: - (ignored)
    > [4 .. ?]: users
    """
    text_col = self.heads[0]
    global_state_col = self.heads[2]
    user_cols = self.heads[4:]

    for user in user_cols:
      self.users[user] = []
      
    all_records = self.sheet.get_all_records()

    for line in all_records:
      text = line[text_col].lower()

      if text == "" or len(text) == 0:
        continue

      global_state = line[global_state_col].lower()

      self.passcodes.append(text)
      
      if is_valid(global_state):
        for user in user_cols:
          user_state = line[user].lower()
          
          if is_valid(user_state):
            self.users[user].append(text)

  def add_user(self, username):
    self.sheet.update_cell(1, len(self.users) + 5, username)
  
  def add_passcode(self, text):
    passcode = text.lower()

    if passcode in self.passcodes:
      return False
    
    self.passcodes.append(passcode)
    self.sheet.update_cell(len(self.passcodes) + 1, 1, passcode)

    return True

  def set_passcode_state(self, user, text, state):
    passcode = text.lower()

    if passcode in self.passcodes:
      row = self.passcodes.index(passcode) + 2
      col = self.heads.index(user) + 1
      
      self.sheet.update_cell(row, col, state)
    
  def get_random_passcode(self, user):
    passcodes = self.users[user]
    
    if len(passcodes) == 0:
      raise NoPasscodeAvailableException("No passcode available for user {}".format(user))

    return passcodes[random.randint(0, len(passcodes) - 1)]


def is_valid(state):
  return len(state) == 0 or state == "valide" or state == ""


class NoPasscodeAvailableException(Exception):
  pass
